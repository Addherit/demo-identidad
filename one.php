<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      Identidad Demo
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">--> 
  <script src="https://kit.fontawesome.com/a2fd7bbf39.js"></script>
  <!-- CSS Files -->
  <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="./assets/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
 <link href="./assets/demo/demo.css" rel="stylesheet" />
 <!-- Include Quill stylesheet -->
<link href="https://cdn.quilljs.com/1.0.0/quill.snow.css" rel="stylesheet">

    <script src="https://cdn.rawgit.com/turbo/justContext.js/master/justcontext.js"></script>
 <style>


.outline {
  outline: 10px solid #404040

}

input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
   /* layout.css Style */
.upload-drop-zone {
  height: 200px;
  border-width: 2px;
  margin-bottom: 20px;
}

/* skin.css Style*/
.upload-drop-zone {
  color: #ccc;
  border-style: dashed;
  border-color: #ccc;
  line-height: 200px;
  text-align: center
}
.upload-drop-zone.drop {
  color: #222;
  border-color: #222;
}
 #mydiv {
  position: absolute;
  z-index: 9;
  text-align: center;
  }
  .jctx {
    display: none;
    z-index: 1000;
    position: absolute;
    overflow: hidden;
    border: 1px solid #595959;
    white-space: nowrap;
    font-family: sans-serif;
    font-size: 12px;
    border-radius: 2px;
    padding: 10px;
	opacity: 0;
	-webkit-transition:opacity 200ms;
    -moz-transition:opacity 200ms;
    -o-transition:opacity 200ms;
    -ms-transition:opacity 200ms;
}

.jctx-black {
    background: black;
    color: white;
}
.jctx-gray {
    background: #212121;
    color: white;
}

.jctx-white {
    background: white;
    color: black;
}

.jctx-white-shadow {
	box-shadow: 0 0 15px grey;
}

.jctx-black-shadow {
	box-shadow: 0 0 15px black;
}

.jctx li {
    padding: 5px 8px;
    cursor: pointer;
}

.jctx li.disabled {
    color: darkgrey;
	cursor: default;
}

.jctx-black li:hover {
    background-color: #284160;
}

.jctx-white li:hover {
    background-color: #81B2D6;
}

.jctx-black li.disabled:hover {
    background-color: #202020;
}

.jctx-white li.disabled:hover {
    background-color: lightgrey;
}

.jctx i {
    padding-right: 6px
}

.jctx hr {
    background-color: grey;
    height: 1px;
    border: 0;
    margin: 2px 0px;
}

#editor-container {
  height: 300px;
}
.inactive .ql-editor {
  padding:0;
  line-height:inherit;
}

.inactive .ql-editor p {
  padding:inherit;
  margin-bottom:10px;
}

.inactive .ql-toolbar {
  display:none;
}

.inactive .ql-container.ql-snow {
  border:none;
  font-family:inherit;
  font-size:inherit;
}
 </style>
</head>





<body>
  <div class="wrapper ">

    <div class="sidebar" data-color="black">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="./mysites.php" class="simple-text logo-mini">
             <img src="http://identidad.com/Images/Site/ID.jpg"> 
        </a>
        <a href="./mysites.php" class="simple-text logo-normal">
            Identidad
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="">
              <i class="fas fa-pager"></i>
            </a>
          </li>
          <br>
          <li>
            <a href="./one-edit.html">
              <i class="fas fa-edit"></i>
            </a>
          </li>
          <br>
          <li>
            <a href="">
              <i class="fas fa-download"></i>
            </a>
          </li>
          <br>
          <li>
            <a href="./typography.html">
                <i class="fas fa-spell-check"></i>
            </a>
          </li>
          <br>
          <li>
            <a href="./timeline2.html">
                <i class="fas fa-hourglass-half"></i>
            </a>
          </li>
          <li class="active-pro">
            <a href="./user.html">
                <i class="fas fa-user"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
   
    <!--main panel-->
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo"></a>
          </div>
       </div>
      </nav>
      <!-- End Navbar -->
        <div class="container-fluid">
          
            <div class="row jctx-host jctx-id-he1 hover " id="he1"style="height: 40%;">
                    <img src="./uploads/header1.png"class="hover" style="height: 100%;width: 100%;"> 
            </div>

            <div class="row  "style="">
                <div class="col-xl-4 jctx-host jctx-id-bi1 hover" id="b1"style="height: 300px;">
                    <img src="./uploads/bi1.png"class="hover" style="height: 100%;width: 100%;"> 
              </div>
                <div class="col-xl-4 jctx-host jctx-id-bi2 hover  " id="b2"style="height: 300px;">
                    <img src="./uploads/bi2.png" class="hover"style="height: 100%;width: 100%;"> 
              </div>
                <div class="col-xl-4 jctx-host jctx-id-bi3 hover  " id="b3"style="height: 300px;">
                    <img src="./uploads/bi3.png"class="hover" style="height: 100%;width: 100%;"> 
              </div>
            </div>

<hr>
            <div class="row"style="border:1px solid white;">
                <div class="col" >
				</div>

                <div class="col jctx-host jctx-id-title" >
				<?php
                  $fp = fopen("title.txt", "r");
                  $linea = fgets($fp);
                  fclose($fp);
                  echo $linea;
                  ?>
				</div>
			
                <div class="col" >
				</div>
			</div>

            <div class="row"style="border:1px solid white;">
                <div class="col-lg-4 jctx-host jctx-id-txt1 hover" id="div">
                  <?php 
                  $fp = fopen("txt1.txt", "r");
                  $linea = fgets($fp);
                  fclose($fp);
                  echo $linea;
                  ?>
                </div>
                <div class="col-lg-4 jctx-host jctx-id-txt2 hover "id="b5">
                  <?php 
                  $fp = fopen("txt2.txt", "r");
                  $linea = fgets($fp);
                  fclose($fp);
                  echo $linea;
                  ?>
                </div>
                <div class="col-lg-4 jctx-host jctx-id-txt3 hover"id="b6">
                  <?php
                  $fp = fopen("txt3.txt", "r");
                  $linea = fgets($fp);
                  fclose($fp);
                  echo $linea;
                  ?>
                </div>
            </div>

            <div class="row jctx-host jctx-id-he2" id="he2"style="border:1px solid white;height: 500px;">
                    <img src="uploads/header2.png"class="hover"style="width:100%;height: 100%;"> 
            </div>

            <div class="row"style="border:1px solid white;">
              <div class="col-xl-6 jctx-host jctx-id-txt4 hover "id="b6">
                <?php
                $fp = fopen("txt4.txt", "r");
                $linea = fgets($fp);
                fclose($fp);
                echo $linea;
                ?>
              </div>
              <div class="col-xl-6 jctx-host jctx-id-bi4"id="b7"style="height: 300px;">
                <img src="./uploads/bi4.png" class="hover"style="height: 100%;width: 100%;"> 
              </div>
            </div>

                <?php
                $fp = fopen("secCount.txt", "r");
                $linea = (int)fgets($fp);
                fclose($fp);
				if($linea != 0){
						switch ($linea) {
								case 1:
								include'section.php';
								break;

								case 2:
								include'section.php';
								include'section.php';
								break;

								case 3:
								include'section.php';
								include'section.php';
								include'section.php';
							    break;
																					

								case 4:
								include'section.php';
								include'section.php';
								include'section.php';
								include'section.php';
								break;

								case 5:
								include'section.php';
								include'section.php';
								include'section.php';
								include'section.php';
								include'section.php';
							    break;
						}


				}
                ?>





        </div>
    </div>
  </div>


<!--Menus-->


<div class=" jctx jctx-id-title jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');document.getElementById('idt').value = 'title';console.log('title');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray" onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="ihe1">Change text</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>





<div class=" jctx jctx-id-he1 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');document.getElementById('id').value = 'ihe1';console.log('ihe1');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray" onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="ihe1">Change Image</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>

<div class=" jctx jctx-id-he2 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');document.getElementById('id').value = 'ihe2';console.log('ihe2');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray" onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="ihe2">Change Image</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>






<div class=" jctx jctx-id-bi1 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');document.getElementById('id').value = 'bi1';console.log('bi1');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray" onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="bi1">Change Image</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>




<div class=" jctx jctx-id-bi2 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');document.getElementById('id').value = 'bi2';console.log('bi2');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray" onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="bi2">Change Image</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>






<div class=" jctx jctx-id-bi3 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');document.getElementById('id').value = 'bi3';console.log('bi3');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray" onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="bi3">Change Image</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>






<div class=" jctx jctx-id-bi4 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');document.getElementById('id').value = 'bi4';console.log('bi4');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray" onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="bi4">Change Image</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>

<!---->


<!--boxes-->
<div class=" jctx jctx-id-txt1 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');document.getElementById('idt').value = 'txt1';console.log('txt1');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray"  onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="txt1">Change Text</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>





<div class=" jctx jctx-id-txt2 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');document.getElementById('idt').value = 'txt2';console.log('txt2');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray"  onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="txt2">Change Text</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>




<div class=" jctx jctx-id-txt3 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');document.getElementById('idt').value = 'txt3';console.log('txt3');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray"  onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="txt3">Change Text</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>



<div class=" jctx jctx-id-txt4 jctx-black jctx-black-shadow">
<div class="row">
     <div class="col"><div class="card jctx-gray" onclick="$('#myModal').modal({ show: false});$('#Modal').modal('show');document.getElementById('idt').value = 'txt4';console.log('txt4');" style="width: 110px; text-align:center; ">
      				  <i class="far fa-file-alt fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;"> Change text</p>
		</div>
	</div>
	<div class="card jctx-gray  "  onclick="$('#myModal').modal({ show: false});$('#exampleModal').modal('show');" style="width: 110px; text-align:center; ">
		<i class="far fa-image fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Change Image</p>
		</div>
	  </div>

	<div class="card jctx-gray"  onclick="addsection()" style="width: 110px;text-align:center; ">
			<i class="far fa-plus-square fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;" >Add Seccion</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="far fa-file-code fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Archivos</p>
		</div>
	</div>
<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-font fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Font</p>
		</div>
	</div>

<div class="card jctx-gray" style="width: 110px;text-align:center; ">
<i class="fas fa-users fa-5x"></i>
		<div class="card-body">
		  <p class="card-text" style="color: white;">Grupos</p>
		</div>
	</div>
  </div>


          <div class="w-100"></div>
          <div class="col">
            <hr>
<ul class="">
  <li data-action="txt4">Change Text</li>
  <li data-action="he1">change size</li>
  <li data-action="new_pkg">New stuff</li>
  <li data-action="new_cls">New class</li>
</ul>
</div>
          <div class="w-100"></div>

      </div>
      </div>





<!-- End Menus-->

<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload  Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
           <div class="panel panel-default">
        <div class="panel-body">

		  <form action = "./upload.php" method = "POST" enctype = "multipart/form-data">
				  		<label class="custom-file-upload">
						         <input type = "file" name = "file"/>
								          <input type = "text" name = "id" id="id" value="d" hidden/>
										  			Explora
															</label>
															         <input type = "submit" value="Subir" class="custom-file-upload"/>
																	 			
      </form>

          <!-- Standar Form 
          <h4>Select files from your computer</h4>
          <form action="upload.php" method="post" enctype="multipart/form-data" id="js-upload-form">
            <div class="form-inline">
              <div class="form-group">
                <input type="file" name="files[]" id="js-upload-files" multiple>
              </div>
              <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit">Upload files</button>
            </div>
          </form>

          Drop Zone 
          <h4>Or drag and drop files below</h4>
          <div class="upload-drop-zone" id="drop-zone">
            Just drag and drop files here
          </div>
        -->


        </div>
      </div>

          </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Text</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
           <div class="panel panel-default">
        <div class="panel-body">

         <input type = "text" name = "idt" id="idt" value="d" hidden/>
<div id="editor" style="height: 300px">
  <p>Hello World!</p>
  <p>Some initial <strong>bold</strong> text</p>
  <p><br></p>
</div>
        </div>
      </div>

          </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary"  onclick="savetext()">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>

      </div>
    </div>
  </div>
</div>






  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js"></script>
  <script src="./assets/js/core/popper.min.js"></script>
  <script src="./assets/js/core/bootstrap.min.js"></script>
  <script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Chart JS -->
  <script src="./assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="./assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/now-ui-dashboard.min.js?v=1.3.0" type="text/javascript"></script>
  <!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
  <script src="./assets/demo/demo.js"></script>
<!-- Include the Quill library -->
<script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>

<!-- Initialize Quill editor -->
<script>
  var quill = new Quill('#editor', {
    theme: 'snow'
  });

  quill.on('editor-change', function(eventName, ...args) {
  if (eventName === 'text-change') {
      var concat = quill.getText();
      console.log(concat.toString());
    // args[0] will be delta
  } else if (eventName === 'selection-change') {
    // args[0] will be old range
  }
});


//Acciones Menus-->
   //Modal
   $('#myModal').modal({ show: false})

function handleMenuAction(evt) {
  console.log(evt)
  switch (evt) {
    case 'ihe1':
      $('#exampleModal').modal('show');
        document.getElementById("id").value = evt;
      break;
    case 'ihe2':
      $('#exampleModal').modal('show');
        document.getElementById("id").value = evt;
      break;
    case 'bi1':
      $('#exampleModal').modal('show');
        document.getElementById("id").value = evt;
      break;
    case 'bi2':
      $('#exampleModal').modal('show');
        document.getElementById("id").value = evt;
      break;
    case 'bi3':
      $('#exampleModal').modal('show');
        document.getElementById("id").value = evt;
      break;
    case 'bi4':
      $('#exampleModal').modal('show');
        document.getElementById("id").value = evt;
      break;
    case 'txt1':
      $('#Modal').modal('show');
        document.getElementById("idt").value = evt;
        console.log(evt);
      break;
    case 'txt2':
      $('#Modal').modal('show');
        document.getElementById("idt").value = evt;
        console.log(evt);
      break;
    case 'txt3':
      $('#Modal').modal('show');
        document.getElementById("idt").value = evt;
        console.log(evt);
      break;
    case 'txt4':
      $('#Modal').modal('show');
        document.getElementById("idt").value = evt;
        console.log(evt);
      break;
    default:
      break;
  }
}


function consultar_datos_cubo() {
    console.log("entro"); 
    $.ajax({
//       url: './gethisto.php',//falta revisar estos paths
       type: 'get',
       dataType: 'json',
    }).done ((lineas) => {                
       $("#div").append(lineas);
   });
}

function savetext() {
    var fila = []
	var linea_single = {}      
    var  box = document.getElementById("idt").value ;
    var editor_content = quill.container.innerHTML;
    linea_single['id'] = box;
    linea_single['editor'] = editor_content;
	fila.push(linea_single)

	console.log(fila)
    $.ajax({
       url: 'savetext.php',//falta revisar estos paths
       type: 'POST',
	//   data: box,
       data:  {fila: fila} ,
       dataType: 'json',
    }).done ((lineas) => {                

	if(lineas == 'ok'){

      $('#Modal').modal('hide');
		console.log(lineas);
	  location.reload(true);
	}
        
   });
}

function addsection() {
    $.ajax({
       url: 'add_section.php',
       type: 'GET',
	   dataType: 'json',
    }).done ((lineas) => {                

	if(lineas == "ok"){
		console.log(lineas);
		location.reload(true);
	}else{
		alert("No puedes agregar mas secciones por el momento");
	}
        
   });
}




//hover

window.addEventListener('mouseover', mouseEnter, false);

window.addEventListener('mouseout', mouseLeave, false);

function mouseEnter(e) {
  /* if hovered node is NOT the registered
  || event listener...
  */
		if (e.target !== e.currentTarget) {
    // Reference hovered element
    var tgt = e.target;

    // Reference the first child with .hover
    var kid = tgt.querySelector('.hover');

    /* if hovered node has class .hover and
    || does NOT have a child with class .hover...
    */
	if (tgt.classList.contains('hover') && !kid) {

      // add class .outline to hovered node
      tgt.classList.add('outline');

      // Otherwise do nothing and end function
    
	} else {
      return;
    
	}

    // Stop the bubbling phase
    e.stopPropagation();
  
		}

}

function mouseLeave(e) {

		if (e.target !== e.currentTarget) {
    var tgt = e.target;
    tgt.classList.remove('outline');
  
		}
  e.stopPropagation();

}


/*
+ function($) {

    'use strict';

    // UPLOAD CLASS DEFINITION
    // ======================

    var dropZone = document.getElementById('drop-zone');
    var uploadForm = document.getElementById('js-upload-form');

    var startUpload = function(files) {
        console.log(files)
    }

    uploadForm.addEventListener('submit', function(e) {
        var uploadFiles = document.getElementById('js-upload-files').files;
        e.preventDefault()

        startUpload(uploadFiles)
    })

    dropZone.ondrop = function(e) {
        e.preventDefault();
        this.className = 'upload-drop-zone';

        startUpload(e.dataTransfer.files)
    }

    dropZone.ondragover = function() {
        this.className = 'upload-drop-zone drop';
        return false;
    }

    dropZone.ondragleave = function() {
        this.className = 'upload-drop-zone';
        return false;
    }

}(jQuery);*/
</script>
</body>




</html>
