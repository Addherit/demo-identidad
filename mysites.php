<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      Mi Sitios
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <script src="https://kit.fontawesome.com/a2fd7bbf39.js"></script>
  <!-- CSS Files -->
  <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="./assets/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
 <link href="./assets/demo/demo.css" rel="stylesheet" />
 <style>
.btn-identidad {
    border-color:#ff008c;
    color:#ff008c;
}
 </style>

</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="black">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="./mysites.php" class="simple-text logo-mini">
             <img src="http://identidad.com/Images/Site/ID.jpg"> 
        </a>
        <a href="./mysites.php" class="simple-text logo-normal">
            Identidad
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="./mysites.php">
              <i class="fas fa-pager"></i>
            </a>
          </li>
          <br>
          <li>
            <a href="">
              <i class="fas fa-user-friends"></i>
            </a>
          </li>
          <br>
          <li>
            <a href="">
              <i class="fas fa-layer-group"></i>
            </a>
          </li>
          <br>
          <li class="active-pro">
            <a href="">
                <i class="fas fa-user"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
    
    <!--main panel-->
    <div class="main-panel" id="main-panel" >
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo"></a>
          </div>
       </div>
      </nav>
      <!-- End Navbar -->
        <div class="container-fluid">
<br>
        <div class="row">

          <div class="col-lg-4">
            
            <div class="card card-chart">
              <div class="card-header">
                <div class="form-row">

                  <div class="col-md-6 mb-3 offset-1">
                    <h4 class="card-title">Nuevo Sitio</h4>
                  </div>

                  <div class="col-md-3 mb-3">
                    <a href="./NewManual.html">
                      <button type="button" class="btn btn-primary" >Crear</button>
                    </a>
                  </div>

                </div>
              </div>
              </div>
            </div>
        </div>


      <div class="content">
        <div class="row">
          
          <div class="col-lg-3">
            <div class="card card-chart">

              <div class="card-header">
                <h4 class="card-title">Prueba el Sitio</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one-edit.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
                </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.php">
                    <img src="http://identidad.com/Images/Site/ID.jpg" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Feb 7 2019 10:07AM | 0 Usuarios | 1.09 Mb
                </div>

              </div>
            </div>
          </div>
          
          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">productiontest</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./second-template.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./cuarto.html">
                    <img src="http://identidad.com/Images/Site/ID.jpg" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Feb 7 2019 10:07AM | 0 Usuarios | 1.09 Mb
                </div>

              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">actinewq</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./tercero-edit.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./tercero.html">
                    <img src="http://identidad.com/Images/Site/ID.jpg" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-danger">Desactivado</p>en Mar 9 2018 10:07AM | 5 Usuarios | 30.9 Mb
                </div>

              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Bienvenidos</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="http://identidad.com/Images/Site/ID.jpg" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Ags 16 2016 06:07AM | 5 Usuarios | 30.9 Mb
                </div>

              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Diseñamexico</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/9/69/2653?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Ags 16 2016 06:07AM | 7 Usuarios | 108.97 Mb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Casa Brugal</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/220/306/6105?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Abr 30 2019 06:11AM | 15 Usuarios | 1,89 Gb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Compartamos</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/63/347/7071?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Ags 28 2019 06:11AM | 19 Usuarios | 1.11 Gb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Colegio de Notarios de la Ciudad de México</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/512/422/10990?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Ags 22 2019 06:11AM | 8 Usuarios | 556.69 Mb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">El Filón</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/220/391/8706?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Mar 21 2019 06:11AM | 4 Usuarios | 445.62 Mb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Áximo</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/220/405/9116?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Ags 19 2019 06:11AM | 4 Usuarios | 450.22 Mb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Galicia</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/512/409/9280?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Jul 31 2019 06:11AM | 6 Usuarios | 136.6 Mb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">LASH BB</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/512/420/9483?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Ags 27 2019 06:11AM | 3 Usuarios | 573.24 Mb
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="card card-chart">
              <div class="card-header">
                <h4 class="card-title">Questum</h4>
                <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="./one.html">Edit</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalEdit">change Info</a>
                    <a class="dropdown-item text-danger" href="#"data-toggle="modal" data-target="#exampleModalDelete">Delete</a>
                  </div>
               </div>
              </div>

              <div class="card-body">
                <div class="chart-area">
                  <a href="./one.html">
                    <img src="https://s3.amazonaws.com/Identidad/220/421/9593?t=4380005252601" style="height: 100%; width: 100%;"> 
                  </a>
                </div>
              </div>

              <div class="card-footer">
                <div class="stats">
                  <p class="text-success">Activado</p>en Mar 22 2019 06:11AM | 1 Usuarios | 1.03 Gb
                </div>
              </div>
            </div>
          </div>


                <?php
                $fp = fopen("manuCount.txt", "r");
                $linea = (int)fgets($fp);
                fclose($fp);
				if($linea != 0){
						switch ($linea) {
								case 1:
								include'newManual.php';
								break;

								case 2:
								include'newManual.php';
								include'newManual.php';
								break;

								case 3:
								include'newManual.php';
								include'newManual.php';
								include'newManual.php';
							    break;
																					

								case 4:
								include'newManual.php';
								include'newManual.php';
								include'newManual.php';
								include'newManual.php';
								break;

								case 5:
								include'newManual.php';
								include'newManual.php';
								include'newManual.php';
								include'newManual.php';
								include'newManual.php';
							    break;
						}


				}
                ?>









        </div>



</div>

</div>





<!-- Modal delete -->
<div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Borrar Manual new test </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Estas a punto de borrar uno de Manuales de testing ¿Estas Seguro?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button"onClick="removemanual()" class="btn btn-danger">Borrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal edit info-->
<div class="modal fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Mi Sitio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
          <input class="form-control" type="text" placeholder="Nombre" value="[Manual Name]">
          <br>
          <input class="form-control" type="text" placeholder="URl" readonly>
         <br> 

          <div class="form-row">
            <div class="col-md-4 mb-3 offset-2">
              <img src="http://identidad.com/Images/Site/ID.jpg"> 
            </div>
            <div class="col-md-6 mb-3">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
          </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div>

</div>
<script>
function removemanual() {
    $.ajax({
       url: 'remove_manual.php',
       type: 'GET',
	   dataType: 'json',
    }).done ((lineas) => {                

	if(lineas == "ok"){
		console.log(lineas);
		location.reload(true);
	}else{
		alert("No puedes eliminar mas manuales");
	}
        
   });
}
</script>
  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js"></script>
  <script src="./assets/js/core/popper.min.js"></script>
  <script src="./assets/js/core/bootstrap.min.js"></script>
  <script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
</script>

  <!--  Notifications Plugin    -->
  <script src="./assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/now-ui-dashboard.min.js?v=1.3.0" type="text/javascript"></script>
</body>

</html>
